<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Picture;
use AppBundle\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * Picture controller.
 */
class PictureController extends Controller
{
    /**
     * Lists all picture entities.
     *
     * @Route("/", name="picture_index")
     * @Method("GET")
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $pictures = $em->getRepository('AppBundle:Picture')
            ->getPicturesDescDate();

        return $this->render('picture/index.html.twig', array(
            'pictures' => $pictures,
            'user' => $this->getUser()
        ));
    }


    /**
     * Creates a new picture entity.
     *
     * @Route("/new", name="picture_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $picture = new Picture();
        $form = $this->createForm('AppBundle\Form\PictureType', $picture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $picture->setUser($this->getUser());
            $picture->setPublishDate(new \DateTime());
            $em->persist($picture);
            $em->flush();

            return $this->redirectToRoute('app_picture_profile', array('id' => $this->getUser()->getId()));
        }

        return $this->render('picture/new.html.twig', array(
            'picture' => $picture,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ));
    }


    /**
     *
     * @Route("profile/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */


    public function profileAction(Request $request, int $id)
    {
        $pictures_user = $this->getDoctrine()->getRepository('AppBundle:Picture')
            ->getPictureUser($id);

        return $this->render('picture/show.html.twig', array(
            'pictures_user' => $pictures_user,
            'user' => $this->getUser(),
        ));
    }

    /**
     *
     * @Route("showpicture/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */


    public function showPictureAction(Request $request, int $id)
    {
        $picture = $this->getDoctrine()->getRepository('AppBundle:Picture')
            ->find($id);

        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $comment->setUser($this->getUser());
            $comment->setPublishDate(new \DateTime());
            $comment->setPicture($picture);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute('app_picture_showpicture', ['id' => $id]);
        }

        $comments = $this->getDoctrine()->getRepository('AppBundle:Comment')
            ->getCommentsDescDate();
        return $this->render('picture/showPicture.html.twig', array(
            'picture' => $picture,
            'user' => $this->getUser(),
            'form' => $form->createView(),
            'comments' => $comments,
        ));
    }

    /**
     * Deletes a picture entity.
     * @Route("delete/{id}")
     * @Method({"GET", "POST", "DELETE"})
     */
    public function deletePictureUserAction(Request $request, int $id)
    {
        $picture_comments = $this->getDoctrine()->getRepository('AppBundle:Comment')
            ->getCommentPictureId($id);
        $em = $this->getDoctrine()->getManager();
        foreach ($picture_comments as $picture_comment) {
            $em->remove($picture_comment);
        }

        $id_one_picture = $this->getDoctrine()->getRepository('AppBundle:Picture')
            ->find($id);
        $em->remove($id_one_picture);
        $em->flush();

        return $this->redirectToRoute('app_picture_profile', ['id' => $id_one_picture->getUser()->getId()]);
    }

}
